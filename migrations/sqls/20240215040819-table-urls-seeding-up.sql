INSERT INTO urls (original_url, encoded_id, title, access_count, last_access)
VALUES ('https://google.com', 'g8', 'Google', 1009, CURRENT_TIMESTAMP),
       ('https://facebook.com', 'g9', 'Facebook', 876, CURRENT_TIMESTAMP),
       ('https://twitter.com', 'ga', 'Twitter', 622, CURRENT_TIMESTAMP),
       ('https://instagram.com', 'gb', 'Instagram', 1328, CURRENT_TIMESTAMP),
       ('https://linkedin.com', 'gc', 'LinkedIn', 392, CURRENT_TIMESTAMP);