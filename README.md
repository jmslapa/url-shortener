# URL SHORTENING

## Introduction
The chosen shortening stratagy, was create an base62 encoded id, based on the primary key of the database table where the url as registered. For this approach, taking into consideration that there is at least 5 ready-to-use implementations in the provided example for this test, I decided to use an existing library to encode/decode the decimals into base62 and expend more time into the business rules and the code architecture. I built the application based in the clean architecture concepts, around the express.js microframework. I also used MySQL as SGBD tool, and used db-migrate library to handle the database versioning and seeding.

## Preparing the enviroment

To serve the application, is mandatory to get Docker installed locally. To make it work, is necessary to create a `.env` file and fill it with the following environment variables:
```
TZ=UTC
ENVIRONMENT=development
DB_HOST=mysql
DB_USER=root
DB_PASS=root
DB_NAME=test
DB_PORT=3306
SHORTEN_URL_PREFIX=http://localhost:3000
```

## Important! 
**Be sure that the ports 3000, 3306 and 8080 are free before proceed to the next step**

<hr>
<br>


## Last Step
After create the `.env` file, all you need to do is run the following command at the user terminal:
`docker compose -up -d`

Now the application server must be running at port 3000, the MySQL instance at the port 3306 and the PHP My Admin client at port 8080.

## Final considerations
I had some issues getting the path aliases to work, so at some point, I just gave up using it. That's not ideal, but with more time I could probably find the right solution. I spent a lot of time with it, and in the end, to save little time, I decided to not implement unit tests, and make feature tests, mocking external dependencies, such as database repositories and web scrapping, and executing the controllers, simulating real http requests. To execute the tests, the command is: `npm run test`.