import { afterMiddlewares, beforeMiddlewares } from './main/config/middlewares'
import { Application } from './main/app'
import { router } from './main/config/router'
import { type RequestHandler } from 'express'
const app = Application.getInstance(
  3000,
  beforeMiddlewares as RequestHandler[],
  afterMiddlewares as RequestHandler[],
  router)
app.init()
app.start()
