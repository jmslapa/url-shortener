import { UrlEntity } from '../../domain/entities'
import { type FetchTitleTag, ShortenUrl as UseCase } from '../../domain/usecases'
import { type UpdateUrl, type CreateUrl } from '../contracts/database'
import { type Dispatcher, type DecimalNumberBaseConverter, type ShortenUrlGenerator, type UrlValidator } from '../contracts/tools'

export class ShortenUrl implements UseCase {
  constructor (
    private readonly urlRepository: CreateUrl & UpdateUrl,
    private readonly decimalNumberBaseConverter: DecimalNumberBaseConverter,
    private readonly shortenUrlGenerator: ShortenUrlGenerator,
    private readonly jobDispatcher: Dispatcher,
    private readonly fetchTitleTagService: FetchTitleTag,
    private readonly urlValidator: UrlValidator,
    private readonly jobDelay: number
  ) {}

  async shorten (params: UseCase.Params): Promise<UseCase.Result> {
    if (!this.urlValidator.isValid(params.url)) {
      return {
        state: UseCase.ResultState.INVALID_URL,
        error: new Error('A valid ULR is required.')
      }
    }

    const entity = UrlEntity.builder()
      .withOriginalUrl(params.url)
      .build()

    const created = await this.urlRepository.create(entity)
    if (created instanceof Error || !created.id) {
      return {
        state: UseCase.ResultState.UNABLE_TO_PERSIST,
        error: created instanceof Error
          ? created
          : new Error('Failed to persist URL in the database')
      }
    }

    this.jobDispatcher.dispatch({
      delay: this.jobDelay,
      job: async () => {
        await this.fetchTitleTagService.fetch({ id: Number(created.id) })
      }
    })

    const encodedId = this.decimalNumberBaseConverter.toBase62(created.id)
    const updated = await this.urlRepository.update({
      id: created.id,
      params: { encodedId }
    })
    if (updated instanceof Error) {
      return {
        state: UseCase.ResultState.UNABLE_TO_SHORTEN,
        error: updated
      }
    }

    const shortUrl = this.shortenUrlGenerator.make(encodedId)

    return {
      state: UseCase.ResultState.SUCCESS,
      shortUrl
    }
  }
}
