import { FetchTitleTag as UseCase } from '../../domain/usecases'
import { type UpdateUrl, type FindUrl } from '../contracts/database'
import { type ScrapTitleTag } from '../contracts/tools'

export class FetchTitleTag implements UseCase {
  constructor (
    private readonly urlRepository: FindUrl & UpdateUrl,
    private readonly scrapper: ScrapTitleTag
  ) {}

  async fetch (params: UseCase.Params): Promise<UseCase.Result> {
    const { id: urlId } = params

    const retrieved = await this.urlRepository.find({ id: urlId })

    if (!retrieved?.originalUrl) {
      return {
        state: UseCase.ResultState.NOT_FOUND
      }
    }

    const title = await this.scrapper.scrapTitleTag(retrieved.originalUrl)

    if (!title) {
      return {
        state: UseCase.ResultState.UNABLE_TO_RETRIEVE
      }
    }

    const updated = await this.urlRepository.update({
      id: urlId,
      params: {
        title
      }
    })

    if (updated instanceof Error) {
      return {
        state: UseCase.ResultState.UNABLE_TO_PERSIST,
        error: updated
      }
    }

    return {
      state: UseCase.ResultState.SUCCESS,
      updatedEntity: updated
    }
  }
}
