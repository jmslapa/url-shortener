import { RedirectToOriginalUrl as UseCase } from '../../domain/usecases'
import { type FindUrl, type UpdateUrl } from '../contracts/database'
import { type DecimalNumberBaseConverter } from '../contracts/tools'

export class RedirectToOriginalUrl implements UseCase {
  constructor (
    private readonly urlRepository: FindUrl & UpdateUrl,
    private readonly decimalNumberBaseConverter: DecimalNumberBaseConverter
  ) {}

  async redirect (params: UseCase.Params): Promise<UseCase.Result> {
    const { encodedId } = params
    let retrieved: FindUrl.Result
    try {
      retrieved = await this.urlRepository.find({ encodedId })
      if (!retrieved) {
        return {
          state: UseCase.ResultState.NOT_FOUND
        }
      }
    } catch (error) {
      return {
        state: UseCase.ResultState.UNABLE_TO_RETRIEVE
      }
    }

    const decodedId = this.decimalNumberBaseConverter.fromBase62(encodedId)
    if (decodedId !== retrieved.id) {
      return {
        state: UseCase.ResultState.NOT_FOUND
      }
    }

    await this.urlRepository.update({
      id: retrieved.id,
      params: {
        accessCount: (retrieved.accessCount ?? 0) + 1,
        lastAccess: new Date()
      }
    })

    return {
      state: UseCase.ResultState.SUCCESS,
      originalUrl: String(retrieved.originalUrl)
    }
  }
}
