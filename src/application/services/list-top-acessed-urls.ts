import { type UrlEntity } from '../../domain/entities'
import { ListTopAccessedUrls as UseCase } from '../../domain/usecases'
import { type FindTopAccessedUrls } from '../contracts/database'

export class ListTopAccessedUrls implements UseCase {
  constructor (
    private readonly urlRepository: FindTopAccessedUrls
  ) {}

  async list (): Promise<UseCase.Result> {
    let list: UrlEntity[]
    try {
      list = await this.urlRepository.findTopAccessedUrls()
    } catch (error) {
      return {
        state: UseCase.ResultState.UNABLE_TO_RETRIEVE
      }
    }

    return {
      state: UseCase.ResultState.SUCCESS,
      list
    }
  }
}
