import { type UrlEntity } from '../../../../domain/entities'

export interface FindTopAccessedUrls {
  findTopAccessedUrls: () => Promise<FindTopAccessedUrls.Result>
}

export namespace FindTopAccessedUrls {
  export type Result = UrlEntity[]

}
