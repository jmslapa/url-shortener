import { type UrlEntity } from '../../../../domain/entities'

export interface CreateUrl {
  create: (params: CreateUrl.Params) => Promise<CreateUrl.Result>
}

export namespace CreateUrl {
  export type Params = UrlEntity

  export type Result = UrlEntity | Error
}
