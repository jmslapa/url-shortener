import { type UrlEntity } from '../../../../domain/entities'

export interface UpdateUrl {
  update: (params: UpdateUrl.Params) => Promise<UpdateUrl.Result>
}

export namespace UpdateUrl {
  export type Params = {
    id: number
    params: {
      encodedId?: string
      accessCount?: number
      lastAccess?: Date
      title?: string
    }
  }

  export type Result = Error | UrlEntity
}
