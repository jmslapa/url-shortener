import { type UrlEntity } from '../../../../domain/entities'

export interface FindUrl {
  find: (params: FindUrl.Params) => Promise<FindUrl.Result>
}

export namespace FindUrl {
  export type Params = {
    id?: number
    encodedId?: string
    originalUrl?: string
  }

  export type Result = UrlEntity | undefined
}
