export interface Dispatcher {
  dispatch: (params: Dispatcher.Params) => Promise<void>
}

export namespace Dispatcher {
  export type Params = {
    job: (...params: []) => Promise<any>
    delay?: number
  }
}
