export interface DecimalNumberBaseConverter {
  toBase62: (decimalNumber: number) => string
  fromBase62: (encodedNumber: string) => number
}
