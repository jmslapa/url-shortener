export interface ShortenUrlGenerator {
  make: (encodedId: string) => string
}
