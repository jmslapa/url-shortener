export interface ScrapTitleTag {
  scrapTitleTag: (url: string) => Promise<string | undefined>
}
