import { type ResultSetHeader } from 'mysql2'
import { type CreateUrl, type FindTopAccessedUrls, type FindUrl, type UpdateUrl } from '../../../application/contracts/database'
import { type DatabaseConnection } from '../connections'
import { type UrlTransformer } from '../transformers'

export class UrlRepository implements FindTopAccessedUrls, FindUrl, CreateUrl, UpdateUrl {
  constructor (
    private readonly connection: DatabaseConnection,
    private readonly transformer: UrlTransformer
  ) {}

  async findTopAccessedUrls (): Promise<FindTopAccessedUrls.Result> {
    const query = `
    SELECT id,
           original_url,
           encoded_id,
           access_count,
           created_at,
           updated_at,
           last_access,
           title
    FROM urls as u
    WHERE u.access_count > 0
    ORDER BY u.access_count DESC
    LIMIT 100;
    `

    const rows = await this.connection.execute<UrlRepository.Row[]>(query)

    return rows.map((row) => this.transformer.toEntity(row))
  }

  async find (params: FindUrl.Params): Promise<FindUrl.Result> {
    const query = `
    SELECT id,
           original_url,
           encoded_id,
           access_count,
           created_at,
           updated_at,
           last_access,
           title
    FROM urls as u
    WHERE u.id = IFNULL(?, u.id)
      AND u.original_url = IFNULL(?, u.original_url)
      AND (u.encoded_id = IFNULL(?, u.encoded_id) OR u.encoded_id is null);
    `

    const firstRow = (await this.connection.execute<UrlRepository.Row[]>(query, [
      params.id ?? null,
      params.originalUrl ?? null,
      params.encodedId ?? null
    ]))
      .shift()

    if (firstRow) {
      return this.transformer.toEntity(firstRow)
    }
  }

  async create (params: CreateUrl.Params): Promise<CreateUrl.Result> {
    const query = `
    INSERT INTO urls (original_url)
    VALUES (?);
    `

    const resultSet = await this.connection.execute<ResultSetHeader>(query, [params.originalUrl])

    if (!resultSet.affectedRows) {
      return new Error('Failed to persist URL in the database')
    }

    const createdEntity = await this.find({ id: resultSet.insertId })

    return createdEntity ?? new Error('Failed to retrieve created URL from the database')
  }

  async update (params: UpdateUrl.Params): Promise<UpdateUrl.Result> {
    const {
      id,
      params: { encodedId, accessCount, lastAccess, title }
    } = params

    const toBeUpdated = await this.find({ id })

    if (!toBeUpdated) {
      return new Error('URL not found')
    }

    const query = `
    UPDATE urls
    SET encoded_id = IFNULL(?, encoded_id),
        access_count = IFNULL(?, access_count),
        last_access = IFNULL(?, last_access),
        title = IFNULL(?, title)
    WHERE id = ?;
    `

    const resultSet = await this.connection.execute<ResultSetHeader>(query, [
      encodedId ?? null,
      accessCount ?? null,
      lastAccess ?? null,
      title ?? null,
      id
    ])

    if (!resultSet.affectedRows) {
      return new Error('Failed to update URL in the database')
    }

    const updated = await this.find({ id })

    return updated ?? new Error('Failed to retrieve updated URL from the database')
  }
}

export namespace UrlRepository {
  export type Row = {
    id: number | null
    original_url: string | null
    encoded_id: string | null
    access_count: number | null
    created_at: Date | null
    updated_at: Date | null
    last_access: Date | null
    title: string | null
  }
}
