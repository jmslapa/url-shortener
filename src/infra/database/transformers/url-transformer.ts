import { type UrlEntity } from '../../../domain/entities'
import { type UrlRepository } from '../repositories/url-repository'

export class UrlTransformer {
  private static instance: UrlTransformer

  private constructor () {}

  static getInstance (): UrlTransformer {
    if (!UrlTransformer.instance) {
      UrlTransformer.instance = new UrlTransformer()
    }
    return UrlTransformer.instance
  }

  toEntity (row: UrlRepository.Row): UrlEntity {
    return {
      id: row.id,
      originalUrl: row.original_url,
      encodedId: row.encoded_id,
      accessCount: row.access_count,
      createdAt: row.created_at,
      updatedAt: row.updated_at,
      lastAccess: row.last_access,
      title: row.title
    }
  }

  toRow (entity: UrlEntity): UrlRepository.Row {
    return {
      id: entity.id,
      original_url: entity.originalUrl,
      encoded_id: entity.encodedId,
      access_count: entity.accessCount,
      created_at: entity.createdAt,
      updated_at: entity.updatedAt,
      last_access: entity.lastAccess,
      title: entity.title
    }
  }
}
