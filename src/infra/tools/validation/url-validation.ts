import { type UrlValidator as Contract } from '../../../application/contracts/tools'

export class UrlValidator implements Contract {
  private static instance: UrlValidator

  private constructor () {}

  static getInstance (): UrlValidator {
    if (!UrlValidator.instance) {
      UrlValidator.instance = new UrlValidator()
    }
    return UrlValidator.instance
  }

  isValid (url: string): boolean {
    try {
      // eslint-disable-next-line no-new
      new URL(url)
      return true
    } catch {
      return false
    }
  }
}
