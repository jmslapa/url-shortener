import { type ScrapTitleTag } from '../../../application/contracts/tools'

export class NativeScrapper implements ScrapTitleTag {
  private static instance: NativeScrapper

  private constructor () {}

  static getInstance (): NativeScrapper {
    if (!NativeScrapper.instance) {
      NativeScrapper.instance = new NativeScrapper()
    }

    return NativeScrapper.instance
  }

  async scrapTitleTag (url: string): Promise<string | undefined> {
    const response = await fetch(url)
    const html = await response.text()
    const title = html.match(/<title>(.*?)<\/title>/)?.[1]

    return title
  }
}
