import { type ShortenUrlGenerator as Contract } from '../../../application/contracts/tools'

export class ShortenUrlGenerator implements Contract {
  constructor (
    private readonly shortenUrlPrefix: string
  ) {}

  make (encodedId: string): string {
    return `${this.shortenUrlPrefix}/${encodedId}`
  }
}
