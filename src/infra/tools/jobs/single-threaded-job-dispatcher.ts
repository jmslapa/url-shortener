import { type Dispatcher } from '../../../application/contracts/tools'

export class SingleThreadedJobDispatcher implements Dispatcher {
  private static instance: SingleThreadedJobDispatcher
  private constructor () {}

  static getInstance (): SingleThreadedJobDispatcher {
    if (!SingleThreadedJobDispatcher.instance) {
      SingleThreadedJobDispatcher.instance = new SingleThreadedJobDispatcher()
    }
    return SingleThreadedJobDispatcher.instance
  }

  async dispatch (params: Dispatcher.Params): Promise<void> {
    const { job, delay } = params
    if (delay) {
      await new Promise(resolve => setTimeout(resolve, delay))
    }

    try {
      await job()
    } catch (error) {
      console.error('Error while executing job:', error)
    }
  }
}
