import base62 from 'base62'
import { type DecimalNumberBaseConverter } from '../../../application/contracts/tools'

export class Base62Adapter implements DecimalNumberBaseConverter {
  private static instance: Base62Adapter

  private constructor () {}

  static getInstance (): Base62Adapter {
    if (!Base62Adapter.instance) {
      Base62Adapter.instance = new Base62Adapter()
    }

    return Base62Adapter.instance
  }

  toBase62 (decimalNumber: number): string {
    return base62.encode(decimalNumber)
  }

  fromBase62 (encodedNumber: string): number {
    return base62.decode(encodedNumber)
  }
}
