import { type Request, type Response } from 'express'
import { type AbstractController } from '../../../presentation/controllers/abstract-controller'

export class ControllerAdapter {
  constructor (
    private readonly controller: AbstractController
  ) {}

  adapt (request: Request, response: Response): void {
    this.controller.handle({
      body: request.body,
      params: request.params,
      query: request.query,
      headers: request.headers
    })
      .then(httpResponse => {
        if (httpResponse.headers) {
          Object.entries(httpResponse.headers).forEach(([key, value]) => {
            response.setHeader(key, value)
          })
        }
        response.status(httpResponse.status)
        response.send(httpResponse.data)
      })
      .catch(error => {
        console.error('Error:', error)
        response.status(500)
          .send({ error: 'Internal server error' })
      })
  }
}
