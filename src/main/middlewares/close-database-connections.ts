import { DatabaseConnections } from '../config/database-connections'

export const closeDatabaseConnections = async (req: any, res: any, next: any): Promise<any> => {
  const connections = Object.values(DatabaseConnections)
  await Promise.all(connections.map(async (connection) => connection.close()))
  next()
}
