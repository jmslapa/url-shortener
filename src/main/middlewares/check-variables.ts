import { variables } from '../config/variables'

export const checkVariables = async (req: any, res: any, next: any): Promise<any> => {
  const missingVariables = Object.keys(variables).filter(variable => variables[variable] === undefined)
  if (missingVariables.length) {
    console.error('Missing environment variables:', missingVariables)
    return res.status(500).json({ error: 'Missing environment variables' })
  }
  next()
}
