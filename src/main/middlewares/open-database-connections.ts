import { DatabaseConnections } from '../config/database-connections'

export const openDatabaseConnections = async (req: any, res: any, next: any): Promise<any> => {
  const connections = Object.values(DatabaseConnections)
  const result = await Promise.allSettled(connections.map(async (connection) => connection.open()))
  const failedConnections = result.filter((promise) => promise.status === 'rejected')
  if (failedConnections.length > 0) {
    res.status(500).json({ error: 'Failed to connect to databases' })
  }

  next()
}
