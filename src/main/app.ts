import express, { type Express, type RequestHandler, type Router } from 'express'
import { type Server } from 'http'

export class Application {
  private static instance: Application
  protected server?: Server
  private readonly express: Express

  private constructor (
    private readonly port: number,
    private readonly beforeMiddlewares: RequestHandler[],
    private readonly afterMiddlewares: RequestHandler[],
    private readonly router: Router
  ) {
    this.express = express()
  }

  public static getInstance (
    port: number,
    beforeMiddlewares: RequestHandler[],
    afterMiddlewares: RequestHandler[],
    router: Router
  ): Application {
    if (!Application.instance) {
      Application.instance = new Application(port, beforeMiddlewares, afterMiddlewares, router)
    }

    return Application.instance
  }

  public init (): void {
    this.express.use(express.json())
    this.express.use(this.beforeMiddlewares)
    this.express.use(this.router)
  }

  public start (): void {
    this.server = this.express.listen(this.port, () => {
      console.log(`Server is running on http://localhost:${this.port}`)
    })
  }
}
