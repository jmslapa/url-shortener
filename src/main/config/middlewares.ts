import { checkVariables } from '../middlewares/check-variables'
import { openDatabaseConnections } from '../middlewares/open-database-connections'

export const beforeMiddlewares: any[] = [
  checkVariables,
  openDatabaseConnections
]

export const afterMiddlewares: any[] = [

]
