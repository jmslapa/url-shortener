import dotenv from 'dotenv'

dotenv.config()

export const variables: Record<string, string | undefined> = {
  timezone: process.env.TZ,
  environment: process.env.ENVIRONMENT,
  dbHost: process.env.DB_HOST,
  dbUser: process.env.DB_USER,
  dbPass: process.env.DB_PASS,
  dbName: process.env.DB_NAME,
  dbPort: process.env.DB_PORT,
  shortenUrlPrefix: process.env.SHORTEN_URL_PREFIX
}
