import { Router, type Request, type Response } from 'express'
import { type AbstractController } from '../../presentation/controllers/abstract-controller'
import { ControllerAdapterFactory, ListTopAccessedUrlsControllerFactory, RedirectToOriginalUrlControllerFactory, ShortenUrlControllerFactory } from '../factories'

export const router: Router = Router()

const adapt = (req: Request, resp: Response, controller: AbstractController): void => {
  ControllerAdapterFactory.getInstance()
    .make(controller)
    .adapt(req, resp)
}

router.get('/', (req, resp) => {
  adapt(req, resp, ShortenUrlControllerFactory.getInstance().make())
})

router.get('/:encodedId', (req, resp) => {
  adapt(req, resp, RedirectToOriginalUrlControllerFactory.getInstance().make())
})

router.get('/reports/top-accessed', (req, resp) => {
  adapt(req, resp, ListTopAccessedUrlsControllerFactory.getInstance().make())
})
