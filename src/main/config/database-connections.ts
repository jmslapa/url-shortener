import { type DatabaseConnection, MysqlConnection } from '../../infra/database/connections'
import { variables } from './variables'

export const DatabaseConnections: Record<string, DatabaseConnection> = {
  default: new MysqlConnection({
    host: variables.dbHost,
    port: Number(variables.dbPort),
    user: variables.dbUser,
    password: variables.dbPass,
    database: variables.dbName
  }, variables.environment ?? 'production')
}
