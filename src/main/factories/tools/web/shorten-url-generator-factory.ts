import { ShortenUrlGenerator } from '../../../../infra/tools'
import { variables } from '../../../config/variables'

export class ShortenUrlGeneratorFactory {
  private static instance: ShortenUrlGeneratorFactory
  private constructor () {}

  public static getInstance (): ShortenUrlGeneratorFactory {
    if (!ShortenUrlGeneratorFactory.instance) {
      ShortenUrlGeneratorFactory.instance = new ShortenUrlGeneratorFactory()
    }

    return ShortenUrlGeneratorFactory.instance
  }

  make (): ShortenUrlGenerator {
    return new ShortenUrlGenerator(String(variables.shortenUrlPrefix))
  }
}
