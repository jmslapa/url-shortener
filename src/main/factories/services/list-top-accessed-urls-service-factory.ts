import { ListTopAccessedUrls } from '../../../application/services'
import { UrlRepositoryFactory } from '../database/repositories/url-repository-factory'

export class ListTopAccessedUrlsServiceFactory {
  private static instance: ListTopAccessedUrlsServiceFactory
  private constructor () {}

  public static getInstance (): ListTopAccessedUrlsServiceFactory {
    if (!ListTopAccessedUrlsServiceFactory.instance) {
      ListTopAccessedUrlsServiceFactory.instance = new ListTopAccessedUrlsServiceFactory()
    }

    return ListTopAccessedUrlsServiceFactory.instance
  }

  public make (): ListTopAccessedUrls {
    return new ListTopAccessedUrls(
      UrlRepositoryFactory.getInstance().make()
    )
  }
}
