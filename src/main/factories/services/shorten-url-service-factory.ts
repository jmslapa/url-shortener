import { ShortenUrl } from '../../../application/services'
import { Base62Adapter } from '../../../infra/libraries'
import { SingleThreadedJobDispatcher, UrlValidator } from '../../../infra/tools'
import { FetchTitleTagServiceFactory, ShortenUrlGeneratorFactory, UrlRepositoryFactory } from '../../factories'

export class ShortenUrlServiceFactory {
  private static instance: ShortenUrlServiceFactory
  private constructor () {}

  public static getInstance (): ShortenUrlServiceFactory {
    if (!ShortenUrlServiceFactory.instance) {
      ShortenUrlServiceFactory.instance = new ShortenUrlServiceFactory()
    }

    return ShortenUrlServiceFactory.instance
  }

  public make (): ShortenUrl {
    return new ShortenUrl(
      UrlRepositoryFactory.getInstance().make(),
      Base62Adapter.getInstance(),
      ShortenUrlGeneratorFactory.getInstance().make(),
      SingleThreadedJobDispatcher.getInstance(),
      FetchTitleTagServiceFactory.getInstance().make(),
      UrlValidator.getInstance(),
      5000
    )
  }
}
