import { UrlRepositoryFactory } from '..'
import { FetchTitleTag } from '../../../application/services'
import { NativeScrapper } from '../../../infra/tools'

export class FetchTitleTagServiceFactory {
  private static instance: FetchTitleTagServiceFactory
  private constructor () {}

  public static getInstance (): FetchTitleTagServiceFactory {
    if (!FetchTitleTagServiceFactory.instance) {
      FetchTitleTagServiceFactory.instance = new FetchTitleTagServiceFactory()
    }

    return FetchTitleTagServiceFactory.instance
  }

  public make (): FetchTitleTag {
    return new FetchTitleTag(
      UrlRepositoryFactory.getInstance().make(),
      NativeScrapper.getInstance()
    )
  }
}
