import { RedirectToOriginalUrl } from '../../../application/services'
import { Base62Adapter } from '../../../infra/libraries'
import { UrlRepositoryFactory } from '../database/repositories/url-repository-factory'

export class RedirectToOriginalUrlServiceFactory {
  private static instance: RedirectToOriginalUrlServiceFactory
  private constructor () {}

  public static getInstance (): RedirectToOriginalUrlServiceFactory {
    if (!RedirectToOriginalUrlServiceFactory.instance) {
      RedirectToOriginalUrlServiceFactory.instance = new RedirectToOriginalUrlServiceFactory()
    }

    return RedirectToOriginalUrlServiceFactory.instance
  }

  public make (): RedirectToOriginalUrl {
    return new RedirectToOriginalUrl(
      UrlRepositoryFactory.getInstance().make(),
      Base62Adapter.getInstance()
    )
  }
}
