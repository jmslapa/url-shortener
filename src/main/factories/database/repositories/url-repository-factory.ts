import { UrlRepository } from '../../../../infra/database/repositories'
import { UrlTransformer } from '../../../../infra/database/transformers'
import { DatabaseConnections } from '../../../config/database-connections'

export class UrlRepositoryFactory {
  private static instance: UrlRepositoryFactory
  private constructor () {}

  public static getInstance (): UrlRepositoryFactory {
    if (!UrlRepositoryFactory.instance) {
      UrlRepositoryFactory.instance = new UrlRepositoryFactory()
    }

    return UrlRepositoryFactory.instance
  }

  public make (): UrlRepository {
    return new UrlRepository(
      DatabaseConnections.default,
      UrlTransformer.getInstance()
    )
  }
}
