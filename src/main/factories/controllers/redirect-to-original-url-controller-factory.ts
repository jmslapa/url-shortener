import { RedirectToOriginalUrlController } from '../../../presentation/controllers'
import { RedirectToOriginalUrlServiceFactory } from '..'

export class RedirectToOriginalUrlControllerFactory {
  private static instance: RedirectToOriginalUrlControllerFactory
  private constructor () {}

  public static getInstance (): RedirectToOriginalUrlControllerFactory {
    if (!RedirectToOriginalUrlControllerFactory.instance) {
      RedirectToOriginalUrlControllerFactory.instance = new RedirectToOriginalUrlControllerFactory()
    }

    return RedirectToOriginalUrlControllerFactory.instance
  }

  public make (): RedirectToOriginalUrlController {
    return new RedirectToOriginalUrlController(
      RedirectToOriginalUrlServiceFactory.getInstance().make()
    )
  }
}
