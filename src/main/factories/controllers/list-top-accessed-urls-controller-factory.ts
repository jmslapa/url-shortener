import { ListTopAccessedUrlsController } from '../../../presentation/controllers'
import { ListTopAccessedUrlsServiceFactory, ShortenUrlGeneratorFactory } from '..'
import { ListTopAcessedUrlsTransformer } from '../../../presentation/transformers/list-top-accessed-urls-transformer'

export class ListTopAccessedUrlsControllerFactory {
  private static instance: ListTopAccessedUrlsControllerFactory
  private constructor () {}

  public static getInstance (): ListTopAccessedUrlsControllerFactory {
    if (!ListTopAccessedUrlsControllerFactory.instance) {
      ListTopAccessedUrlsControllerFactory.instance = new ListTopAccessedUrlsControllerFactory()
    }

    return ListTopAccessedUrlsControllerFactory.instance
  }

  public make (): ListTopAccessedUrlsController {
    return new ListTopAccessedUrlsController(
      ListTopAccessedUrlsServiceFactory.getInstance().make(),
      new ListTopAcessedUrlsTransformer(
        ShortenUrlGeneratorFactory.getInstance().make()
      )
    )
  }
}
