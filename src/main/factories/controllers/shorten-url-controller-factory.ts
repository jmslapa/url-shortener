import { ShortenUrlController } from '../../../presentation/controllers'
import { ShortenUrlServiceFactory } from '../../factories'

export class ShortenUrlControllerFactory {
  private static instance: ShortenUrlControllerFactory
  private constructor () {}

  public static getInstance (): ShortenUrlControllerFactory {
    if (!ShortenUrlControllerFactory.instance) {
      ShortenUrlControllerFactory.instance = new ShortenUrlControllerFactory()
    }

    return ShortenUrlControllerFactory.instance
  }

  public make (): ShortenUrlController {
    return new ShortenUrlController(
      ShortenUrlServiceFactory.getInstance().make()
    )
  }
}
