import { ControllerAdapter } from '../../../infra/frameworks/express/controller-adapter'
import { type AbstractController } from '../../../presentation/controllers/abstract-controller'

export class ControllerAdapterFactory {
  private static instance: ControllerAdapterFactory
  private constructor () {}

  public static getInstance (): ControllerAdapterFactory {
    if (!ControllerAdapterFactory.instance) {
      ControllerAdapterFactory.instance = new ControllerAdapterFactory()
    }

    return ControllerAdapterFactory.instance
  }

  public make (controller: AbstractController): ControllerAdapter {
    return new ControllerAdapter(controller)
  }
}
