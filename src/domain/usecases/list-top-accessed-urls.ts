import { type UrlEntity } from '../entities'

export interface ListTopAccessedUrls {
  list: () => Promise<ListTopAccessedUrls.Result>
}

export namespace ListTopAccessedUrls {
  export type Result = {
    state: ResultState
    list?: UrlEntity[]
    error?: Error
  }

  export enum ResultState {
    SUCCESS,
    UNABLE_TO_RETRIEVE
  }
}
