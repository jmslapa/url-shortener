export interface RedirectToOriginalUrl {
  redirect: (params: RedirectToOriginalUrl.Params) => Promise<RedirectToOriginalUrl.Result>
}

export namespace RedirectToOriginalUrl {
  export type Params = {
    encodedId: string
  }

  export type Result = {
    state: ResultState
    originalUrl?: string
    error?: Error
  }

  export enum ResultState {
    SUCCESS,
    NOT_FOUND,
    UNABLE_TO_RETRIEVE
  }
}
