import { type UrlEntity } from '../entities'

export interface FetchTitleTag {
  fetch: (params: FetchTitleTag.Params) => Promise<FetchTitleTag.Result>
}

export namespace FetchTitleTag {
  export type Params = {
    id: number
  }

  export type Result = {
    state: ResultState
    updatedEntity?: UrlEntity
    error?: Error
  }

  export enum ResultState {
    SUCCESS,
    NOT_FOUND,
    UNABLE_TO_RETRIEVE,
    UNABLE_TO_PERSIST
  }
}
