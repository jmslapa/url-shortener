export interface ShortenUrl {
  shorten: (params: ShortenUrl.Params) => Promise<ShortenUrl.Result>
}

export namespace ShortenUrl {
  export type Params = {
    url: string
  }

  export type Result = {
    state: ResultState
    shortUrl?: string
    error?: Error
  }

  export enum ResultState {
    SUCCESS,
    INVALID_URL,
    UNABLE_TO_PERSIST,
    UNABLE_TO_SHORTEN
  }
}
