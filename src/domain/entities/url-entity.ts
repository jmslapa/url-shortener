export interface UrlEntity {
  id: number | null
  originalUrl: string | null
  encodedId: string | null
  accessCount: number | null
  createdAt: Date | null
  updatedAt: Date | null
  lastAccess: Date | null
  title: string | null
}

export namespace UrlEntity {
  export const builder = (): Builder => new Builder()

  export class Builder {
    private readonly entity: UrlEntity

    constructor () {
      this.entity = {
        id: null,
        originalUrl: null,
        encodedId: null,
        accessCount: null,
        createdAt: null,
        updatedAt: null,
        lastAccess: null,
        title: null
      }
    }

    withId (id: number): this {
      this.entity.id = id
      return this
    }

    withOriginalUrl (originalUrl: string): this {
      this.entity.originalUrl = originalUrl
      return this
    }

    withEncodedId (encodedId: string): this {
      this.entity.encodedId = encodedId
      return this
    }

    withAccessCount (accessCount: number): this {
      this.entity.accessCount = accessCount
      return this
    }

    withCreatedAt (createdAt: Date): this {
      this.entity.createdAt = createdAt
      return this
    }

    withUpdatedAt (updatedAt: Date): this {
      this.entity.updatedAt = updatedAt
      return this
    }

    withLastAccess (lastAccess: Date): this {
      this.entity.lastAccess = lastAccess
      return this
    }

    withTitle (title: string): this {
      this.entity.title = title
      return this
    }

    build (): UrlEntity {
      return this.entity
    }
  }
}
