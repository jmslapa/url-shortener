import { type ShortenUrlGenerator } from '../../application/contracts/tools'
import { type UrlEntity } from '../../domain/entities'
import { type ListTopAccessedUrlsController } from '../controllers'

export class ListTopAcessedUrlsTransformer {
  constructor (
    private readonly shortenUrlGenerator: ShortenUrlGenerator
  ) {}

  toResponseBody (list: UrlEntity[]): ListTopAccessedUrlsController.ResponseBody {
    return list.map((url) => ({
      title: String(url.title),
      url: this.shortenUrlGenerator.make(String(url.encodedId)),
      accessCount: Number(url.accessCount)
    }))
  }
}
