export interface Request {
  body?: Record<string, any>
  params?: Record<string, any>
  query?: Record<string, any>
  headers?: Record<string, any>
}
