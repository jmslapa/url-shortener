import { type HttpStatusCode } from '../../enums'

export interface Response <T = any> {
  status: HttpStatusCode
  data?: T | ErrorResponseBody
  headers?: Record<string, string>
}

export interface ErrorResponseBody {
  message: string
}
