import { type Request, type Response } from '../models/http'
import { AbstractController } from './abstract-controller'
import { HttpStatusCode } from '../enums'
import { RedirectToOriginalUrl } from '../../domain/usecases'

export class RedirectToOriginalUrlController extends AbstractController {
  constructor (
    private readonly RedirectToOriginalUrlService: RedirectToOriginalUrl
  ) {
    super()
  }

  override async perform (request: Request): Promise<Response> {
    const encodedId = request.params?.encodedId

    if (!encodedId) {
      return {
        status: HttpStatusCode.BAD_REQUEST,
        data: {
          message: 'Missing parameter: encodedId.'
        }
      }
    }

    const result = await this.RedirectToOriginalUrlService.redirect({ encodedId })

    return this.handleResult(result)
  }

  private handleResult (result: RedirectToOriginalUrl.Result): Response {
    const dict: Record<RedirectToOriginalUrl.ResultState, Response> = {
      [RedirectToOriginalUrl.ResultState.SUCCESS]: {
        status: HttpStatusCode.FOUND,
        headers: { Location: String(result.originalUrl) }
      },
      [RedirectToOriginalUrl.ResultState.NOT_FOUND]: {
        status: HttpStatusCode.NOT_FOUND,
        data: {
          message: 'URL not found.'
        }
      },
      [RedirectToOriginalUrl.ResultState.UNABLE_TO_RETRIEVE]: {
        status: HttpStatusCode.INTERNAL_SERVER_ERROR,
        data: {
          message: 'An error occurred while trying retrieve the URL.'
        }
      }
    }

    return dict[result.state]
  }
}
