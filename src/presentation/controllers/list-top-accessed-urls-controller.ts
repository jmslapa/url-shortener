import { type Request, type Response } from '../models/http'
import { AbstractController } from './abstract-controller'
import { HttpStatusCode } from '../enums'
import { ListTopAccessedUrls } from '../../domain/usecases'
import { type ListTopAcessedUrlsTransformer } from '../transformers/list-top-accessed-urls-transformer'

export class ListTopAccessedUrlsController extends AbstractController {
  constructor (
    private readonly ListTopAccessedUrlsService: ListTopAccessedUrls,
    private readonly responseBodyTransormer: ListTopAcessedUrlsTransformer
  ) {
    super()
  }

  override async perform (request: Request): Promise<Response<ListTopAccessedUrlsController.ResponseBody>> {
    const result = await this.ListTopAccessedUrlsService.list()

    return this.handleResult(result)
  }

  private handleResult (result: ListTopAccessedUrls.Result): Response<ListTopAccessedUrlsController.ResponseBody> {
    const dict: Record<ListTopAccessedUrls.ResultState, Response<ListTopAccessedUrlsController.ResponseBody>> = {
      [ListTopAccessedUrls.ResultState.SUCCESS]: {
        status: HttpStatusCode.OK,
        data: this.responseBodyTransormer.toResponseBody(result.list ?? [])
      },

      [ListTopAccessedUrls.ResultState.UNABLE_TO_RETRIEVE]: {
        status: HttpStatusCode.INTERNAL_SERVER_ERROR,
        data: {
          message: 'An error occurred while trying retrieve the top accessed URLs.'
        }
      }
    }

    return dict[result.state]
  }
}

export namespace ListTopAccessedUrlsController {
  export type ResponseBody = Array<{
    title: string
    url: string
    accessCount: number
  }>
}
