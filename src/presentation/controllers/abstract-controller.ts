import { HttpStatusCode } from '../enums'
import { type Request, type Response } from '../models/http'

export abstract class AbstractController<T = any> {
  abstract perform (request: Request): Promise<Response<T>>

  async handle (request: Request): Promise<Response<T>> {
    try {
      const response = await this.perform(request)
      return response
    } catch (error) {
      console.error('Unexpected Error:', error)
      return {
        status: HttpStatusCode.INTERNAL_SERVER_ERROR,
        data: { error: 'Internal server error' } as any
      }
    }
  }
}
