import { type Request, type Response } from '../models/http'
import { AbstractController } from './abstract-controller'
import { HttpStatusCode } from '../enums'
import { ShortenUrl } from '../../domain/usecases'

type ResponseBody = {
  url: string
}
export class ShortenUrlController extends AbstractController<ResponseBody> {
  constructor (
    private readonly shortenUrlService: ShortenUrl
  ) {
    super()
  }

  override async perform (request: Request): Promise<Response<ResponseBody>> {
    const url = request.body?.url

    if (!url) {
      return {
        status: HttpStatusCode.UNPROCESSABLE_ENTITY,
        data: {
          message: 'A valid ULR is required.'
        }
      }
    }

    const result = await this.shortenUrlService.shorten({ url })

    return this.handleResult(result)
  }

  private handleResult (result: ShortenUrl.Result): Response<ResponseBody> {
    const dict: Record<ShortenUrl.ResultState, Response<ResponseBody>> = {
      [ShortenUrl.ResultState.SUCCESS]: {
        status: HttpStatusCode.OK,
        data: {
          url: String(result.shortUrl)
        }
      },
      [ShortenUrl.ResultState.INVALID_URL]: {
        status: HttpStatusCode.BAD_REQUEST,
        data: {
          message: 'A valid ULR is required.'
        }
      },
      [ShortenUrl.ResultState.UNABLE_TO_PERSIST]: {
        status: HttpStatusCode.INTERNAL_SERVER_ERROR,
        data: {
          message: 'An error occurred while trying to shorten the URL.'
        }
      },
      [ShortenUrl.ResultState.UNABLE_TO_SHORTEN]: {
        status: HttpStatusCode.INTERNAL_SERVER_ERROR,
        data: {
          message: 'An error occurred while trying to shorten the URL.'
        }
      }
    }

    return dict[result.state]
  }
}
