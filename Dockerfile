FROM node:18.17.0-alpine

WORKDIR /usr/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000

CMD npm run db:migrate && npm run build && npm run start