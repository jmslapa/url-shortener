import { type CreateUrl, type FindUrl, type UpdateUrl } from '../../src/application/contracts/database'
import { ShortenUrl as ShortenUrlService } from '../../src/application/services'
import { UrlEntity } from '../../src/domain/entities'
import { type FetchTitleTag, type ShortenUrl } from '../../src/domain/usecases'
import { Base62Adapter } from '../../src/infra/libraries'
import { SingleThreadedJobDispatcher, UrlValidator } from '../../src/infra/tools'
import { variables } from '../../src/main/config/variables'
import { ShortenUrlGeneratorFactory } from '../../src/main/factories'
import { ShortenUrlController } from '../../src/presentation/controllers'
import { HttpStatusCode } from '../../src/presentation/enums'

describe('Shorten URL', () => {
  let repositoryMock: FindUrl & CreateUrl & UpdateUrl
  let fetchToolStub: FetchTitleTag
  let service: ShortenUrl
  let controller: ShortenUrlController
  const jobDelay = 2000

  beforeAll(() => {
    repositoryMock = {
      find: jest.fn(),
      create: jest.fn(),
      update: jest.fn()
    }

    fetchToolStub = {
      fetch: jest.fn()
    }

    service = new ShortenUrlService(
      repositoryMock,
      Base62Adapter.getInstance(),
      ShortenUrlGeneratorFactory.getInstance().make(),
      SingleThreadedJobDispatcher.getInstance(),
      fetchToolStub,
      UrlValidator.getInstance(),
      jobDelay
    )

    controller = new ShortenUrlController(service)
  })

  it('should return 422 if no URL is provided', async () => {
    const response = await controller.handle({ body: {} })

    expect(response).toEqual({
      status: HttpStatusCode.UNPROCESSABLE_ENTITY,
      data: {
        message: 'A valid ULR is required.'
      }
    })
  })

  it('should return 200 if URL is provided', async () => {
    const createSpy = jest.spyOn(repositoryMock, 'create')
    const updateSpy = jest.spyOn(repositoryMock, 'update')
    const fetchSpy = jest.spyOn(fetchToolStub, 'fetch')
    const id = 1000
    const encodedId = 'g8'
    const url = 'https://google.com'

    const toCreateDummyEntity = UrlEntity.builder()
      .withId(id)
      .withOriginalUrl(url)
      .withAccessCount(0)
      .build()

    createSpy.mockReturnValue(Promise.resolve(toCreateDummyEntity))

    const response = await controller.handle({ body: { url } })

    expect(fetchSpy).toHaveBeenCalledTimes(0)
    await new Promise(resolve => setTimeout(resolve, jobDelay))
    expect(fetchSpy).toHaveBeenCalledWith({ id })

    expect(updateSpy).toHaveBeenCalledWith({
      id,
      params: { encodedId }
    })

    expect(response).toEqual({
      status: HttpStatusCode.OK,
      data: {
        url: `${variables.shortenUrlPrefix}/${encodedId}`
      }
    })

    createSpy.mockRestore()
    updateSpy.mockRestore()
    fetchSpy.mockRestore()
  })

  it('should return 400 if URL is invalid', async () => {
    const response = await controller.handle({ body: { url: 'invalid_url' } })
    expect(response).toEqual({
      status: HttpStatusCode.BAD_REQUEST,
      data: {
        message: 'A valid ULR is required.'
      }
    })
  })

  it('should return 500 if an error occurs while trying to create the URL', async () => {
    jest.spyOn(repositoryMock, 'create').mockReturnValue(
      Promise.resolve(new Error('An error occurred while trying to shorten the URL.'))
    )

    jest.spyOn(repositoryMock, 'update').mockReturnValue(
      Promise.resolve(new Error('An error occurred while trying to shorten the URL.'))
    )
    const response = await controller.handle({ body: { url: 'https://google.com.br' } })
    expect(response).toEqual({
      status: HttpStatusCode.INTERNAL_SERVER_ERROR,
      data: {
        message: 'An error occurred while trying to shorten the URL.'
      }
    })
  })

  it('should return 500 if an error occurs while trying to update the URL with the encoded id', async () => {
    jest.spyOn(repositoryMock, 'update').mockReturnValue(
      Promise.resolve(new Error('An error occurred while trying to shorten the URL.'))
    )

    const response = await controller.handle({ body: { url: 'https://google.com.br' } })
    expect(response).toEqual({
      status: HttpStatusCode.INTERNAL_SERVER_ERROR,
      data: {
        message: 'An error occurred while trying to shorten the URL.'
      }
    })
  })

  it('should return 500 if an error occurs while trying to update the URL with the encoded id', async () => {
    jest.spyOn(repositoryMock, 'update').mockReturnValue(
      Promise.resolve(new Error('An error occurred while trying to shorten the URL.'))
    )

    const response = await controller.handle({ body: { url: 'https://google.com.br' } })
    expect(response).toEqual({
      status: HttpStatusCode.INTERNAL_SERVER_ERROR,
      data: {
        message: 'An error occurred while trying to shorten the URL.'
      }
    })
  })

  it('should return 500 if an unexpected error occurs', async () => {
    jest.spyOn(service, 'shorten').mockImplementation(async () => {
      throw new Error('Unexpected error')
    })

    const response = await controller.handle({ body: { url: 'https://google.com.br' } })
    expect(response).toEqual({
      status: HttpStatusCode.INTERNAL_SERVER_ERROR,
      data: {
        error: 'Internal server error'
      }
    })
  })
})
