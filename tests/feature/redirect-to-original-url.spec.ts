import { type FindUrl, type UpdateUrl } from '../../src/application/contracts/database'
import { RedirectToOriginalUrl } from '../../src/application/services'
import { UrlEntity } from '../../src/domain/entities'
import { Base62Adapter } from '../../src/infra/libraries'
import { RedirectToOriginalUrlController } from '../../src/presentation/controllers'
import { HttpStatusCode } from '../../src/presentation/enums'

describe('Shorten URL', () => {
  let repositoryMock: FindUrl & UpdateUrl
  let service: RedirectToOriginalUrl
  let controller: RedirectToOriginalUrlController

  beforeAll(() => {
    repositoryMock = {
      find: jest.fn(),
      update: jest.fn()
    }

    service = new RedirectToOriginalUrl(
      repositoryMock,
      Base62Adapter.getInstance()
    )

    controller = new RedirectToOriginalUrlController(service)
  })

  it('should return 400 if no id is provided', async () => {
    const response = await controller.handle({ params: {} })

    expect(response).toEqual({
      status: HttpStatusCode.BAD_REQUEST,
      data: {
        message: 'Missing parameter: encodedId.'
      }
    })
  })

  it('should return 302 if id is provided', async () => {
    const findSpy = jest.spyOn(repositoryMock, 'find')
    const encodedId = 'g8'
    const url = 'https://google.com'

    findSpy.mockReturnValue(Promise.resolve(UrlEntity.builder()
      .withId(1000)
      .withOriginalUrl(url)
      .build()))

    const response = await controller.handle({ params: { encodedId } })

    expect(response).toEqual({
      status: HttpStatusCode.FOUND,
      headers: { Location: url }
    })
  })

  it('should return 404 if encodedId is not found', async () => {
    const findSpy = jest.spyOn(repositoryMock, 'find')
    const encodedId = 'g8'

    findSpy.mockReturnValue(Promise.resolve(undefined))

    const response = await controller.handle({ params: { encodedId } })

    expect(response).toEqual({
      status: HttpStatusCode.NOT_FOUND,
      data: {
        message: 'URL not found.'
      }
    })
  })

  it('should return 404 if encodedId is found but is not iqual to id when decoded', async () => {
    const findSpy = jest.spyOn(repositoryMock, 'find')
    const encodedId = 'g8'
    const url = 'https://google.com'

    findSpy.mockReturnValue(Promise.resolve(UrlEntity.builder()
      .withId(1)
      .withOriginalUrl(url)
      .build()))

    const response = await controller.handle({ params: { encodedId } })

    expect(response).toEqual({
      status: HttpStatusCode.NOT_FOUND,
      data: {
        message: 'URL not found.'
      }
    })
  })

  it('should return 500 if an error occurs while trying to retrieve the URL', async () => {
    const findSpy = jest.spyOn(repositoryMock, 'find')
    const encodedId = 'g8'

    findSpy.mockRejectedValue(new Error('Any error'))

    const response = await controller.handle({ params: { encodedId } })

    expect(response).toEqual({
      status: HttpStatusCode.INTERNAL_SERVER_ERROR,
      data: {
        message: 'An error occurred while trying retrieve the URL.'
      }
    })
  })

  it('should return 500 if an unexpected error occurs', async () => {
    const encodedId = 'g8'
    jest.spyOn(service, 'redirect').mockImplementation(async () => {
      throw new Error('Unexpected error')
    })

    const response = await controller.handle({ params: { encodedId } })
    expect(response).toEqual({
      status: HttpStatusCode.INTERNAL_SERVER_ERROR,
      data: {
        error: 'Internal server error'
      }
    })
  })
})
