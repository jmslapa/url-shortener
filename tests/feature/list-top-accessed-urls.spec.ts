import { type FindTopAccessedUrls } from '../../src/application/contracts/database'
import { ListTopAccessedUrls } from '../../src/application/services'
import { ListTopAccessedUrlsController } from '../../src/presentation/controllers'
import { HttpStatusCode } from '../../src/presentation/enums'
import { ShortenUrlGeneratorFactory } from '../../src/main/factories'
import { ListTopAcessedUrlsTransformer } from '../../src/presentation/transformers/list-top-accessed-urls-transformer'

describe('List top acessed urls', () => {
  let repositoryMock: FindTopAccessedUrls
  let service: ListTopAccessedUrls
  let controller: ListTopAccessedUrlsController

  beforeAll(() => {
    repositoryMock = {
      findTopAccessedUrls: jest.fn()
    }

    service = new ListTopAccessedUrls(
      repositoryMock
    )

    controller = new ListTopAccessedUrlsController(
      service,
      new ListTopAcessedUrlsTransformer(
        ShortenUrlGeneratorFactory.getInstance().make()
      )
    )
  })

  it('should return 200 if theres not error on database layer', async () => {
    const findSpy = jest.spyOn(repositoryMock, 'findTopAccessedUrls')

    findSpy.mockReturnValue(Promise.resolve([]))

    const response = await controller.handle({})

    expect(response).toEqual({
      status: HttpStatusCode.OK,
      data: []
    })
  })

  it('should return 500 if any error happens in the database layer', async () => {
    const findSpy = jest.spyOn(repositoryMock, 'findTopAccessedUrls')

    findSpy.mockReturnValue(Promise.reject(new Error('Any error')))

    const response = await controller.handle({})

    expect(response).toEqual({
      status: HttpStatusCode.INTERNAL_SERVER_ERROR,
      data: {
        message: 'An error occurred while trying retrieve the top accessed URLs.'
      }
    })
  })

  it('should return 500 if an unexpected error occurs', async () => {
    jest.spyOn(service, 'list').mockImplementation(async () => {
      throw new Error('Unexpected error')
    })

    const response = await controller.handle({})
    expect(response).toEqual({
      status: HttpStatusCode.INTERNAL_SERVER_ERROR,
      data: {
        error: 'Internal server error'
      }
    })
  })
})
